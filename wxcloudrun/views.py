import logging
from rest_framework import viewsets
from wxcloudrun.models import Counters
from wxcloudrun.serializers import CountersSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

logger = logging.getLogger("log")


class CustomPagination(PageNumberPagination):
    page_size = 10  # 每页显示的对象数量
    page_query_param = "page"  # URL 中传递页码的参数名
    page_size_query_param = "page_size"  # URL 中传递每页数量的参数名
    max_page_size = 15  # 每页最大显示的对象数量


class CountersViewSet(viewsets.ModelViewSet):
    queryset = Counters.objects.all()
    serializer_class = CountersSerializer
    pagination_class = CustomPagination
