from rest_framework import serializers
from .models import Counters


class CountersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Counters
